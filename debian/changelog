python-novaclient (2:18.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:37:37 +0200

python-novaclient (2:18.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 Sep 2022 10:12:28 +0200

python-novaclient (2:17.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 14:44:39 +0100

python-novaclient (2:17.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Feb 2022 22:02:43 +0100

python-novaclient (2:17.6.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 17:49:50 +0200

python-novaclient (2:17.6.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 04 Sep 2021 15:36:41 +0200

python-novaclient (2:17.4.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 11:21:24 +0200

python-novaclient (2:17.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: 4.5.1.
  * Debhelper 11.
  * Removed python3-simplejson from (build-)depends as per upstream.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Mar 2021 21:42:08 +0100

python-novaclient (2:17.2.1-2) unstable; urgency=medium

  * Fixed bash-completion script installation.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 11:01:07 +0200

python-novaclient (2:17.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Sep 2020 12:07:00 +0200

python-novaclient (2:17.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 23:26:21 +0200

python-novaclient (2:17.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Sat, 11 Apr 2020 13:21:37 +0200

python-novaclient (2:15.1.0-3) unstable; urgency=medium

  * Make the tests use python3-novaclient (Closes: #937957).

 -- Thomas Goirand <zigo@debian.org>  Sun, 19 Jan 2020 14:07:55 +0100

python-novaclient (2:15.1.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 01:41:27 +0200

python-novaclient (2:15.1.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 00:21:05 +0200

python-novaclient (2:13.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 14:10:19 +0200

python-novaclient (2:13.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Mar 2019 20:18:52 +0100

python-novaclient (2:11.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 00:21:37 +0200

python-novaclient (2:11.0.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove XS-Testsuite field, not needed anymore
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using Python 3 to build the sphinx doc.
  * Removed fix-console-log-encoding.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 11:59:56 +0200

python-novaclient (2:9.1.1-3) unstable; urgency=medium

  * Python 3 has now priority over Python 2.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Mar 2018 13:13:48 +0100

python-novaclient (2:9.1.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:54:35 +0000

python-novaclient (2:9.1.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Feb 2018 21:04:54 +0000

python-novaclient (2:9.1.0-3) unstable; urgency=medium

  * Add fix-console-log-encoding.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 07 Feb 2018 09:40:02 +0100

python-novaclient (2:9.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Nov 2017 23:52:38 +0000

python-novaclient (2:9.1.0-1) experimental; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Prefixing all debhelper files.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Remove Handle_error_response_for_webob_ge_1.6.0.patch applied upstream.
  * Using pkgos-dh_auto_{install,test}.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Oct 2017 21:56:31 +0000

python-novaclient (2:3.3.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * Added Handle_error_response_for_webob_ge_1.6.0.patch (Closes: #830304).

 -- Thomas Goirand <zigo@debian.org>  Tue, 26 Jul 2016 16:35:34 +0200

python-novaclient (2:3.3.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 13:46:14 +0000

python-novaclient (2:3.3.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 02 Apr 2016 15:18:05 +0200

python-novaclient (2:3.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: 3.9.7 (no change).
  * Some fixes in debian/copyright.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Mar 2016 12:48:52 +0100

python-novaclient (2:3.2.0-3) experimental; urgency=medium

  [ Corey Bryant ]
  * d/rules: Use PYTHON3S instead of hardcoded 3.4 for override_dh_auto_test.

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 04:06:13 +0000

python-novaclient (2:3.2.0-2) experimental; urgency=medium

  * Added build-depends version of python-keyring to 7.2, adding runtime
    depends to python-keyring >= 7.2, after experiencing failures on the
    "nova" cli with version 6.1.

 -- Thomas Goirand <zigo@debian.org>  Fri, 29 Jan 2016 13:28:12 +0000

python-novaclient (2:3.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 18 Jan 2016 10:37:46 +0000

python-novaclient (2:2.30.1-4) unstable; urgency=medium

  * override_dh_python3 to fix Py3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Oct 2015 23:18:59 +0000

python-novaclient (2:2.30.1-3) unstable; urgency=medium

  * Uploading to unstable.
  * Disable Python 3.5 unit tests.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 22:13:33 +0000

python-novaclient (2:2.30.1-2) experimental; urgency=medium

  * Added Python3 support.
  * Added a -doc package.

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Oct 2015 11:08:35 +0200

python-novaclient (2:2.30.1-1) experimental; urgency=medium

  * New upstream release.
  * d/control: Bump minimum python-tempest-lib version to 0.8.0.
  * d/control: Update uploaders.

 -- Corey Bryant <corey.bryant@canonical.com>  Mon, 28 Sep 2015 10:17:01 -0400

python-novaclient (2:2.29.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed bash-completion script path.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Sep 2015 18:56:28 +0200

python-novaclient (2:2.26.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
    - d/control: Align requirements with upstream.
    - d/gbp.conf: Update gbp configuration for liberty branch.
  * Converge Ubuntu and Debian package:
    - d/tests/*: Add DEP 8 tests from Ubuntu.

 -- Corey Bryant <corey.bryant@canonical.com>  Tue, 16 Jun 2015 17:05:49 +0100

python-novaclient (2:2.23.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed new (build-)dependencies for this release
  * Standards-Version: 3.9.6.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Apr 2015 18:48:05 +0200

python-novaclient (2:2.17.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Apr 2014 10:13:07 +0800

python-novaclient (2:2.17.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed new (build-)dependencies for this release.
  * Using testr and subunit with better unit test output.

 -- Thomas Goirand <zigo@debian.org>  Fri, 28 Mar 2014 18:49:42 +0800

python-novaclient (2:2.16.0-1) unstable; urgency=medium

  * New upstream release.
  * Using testr to run tests.
  * Standards-Version: is now 3.9.5.
  * Drop gnome keyring patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Fri, 07 Mar 2014 15:38:24 +0800

python-novaclient (2:2.15.0-2) unstable; urgency=medium

  * Applied upstream patch for deprecated Gnome keyring API (Closes: #728470).
  * Removes .testrepository and subunit.log on clean.

 -- Thomas Goirand <zigo@debian.org>  Sat, 04 Jan 2014 01:38:05 +0800

python-novaclient (2:2.15.0-1) unstable; urgency=low

  * New upstream release.
  * Review all new (build-)depends.
  * Fixed VCS URLs to be canonical.
  * Ran wrap-and-sort.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 May 2013 14:07:00 +0800

python-novaclient (2:2.13.0-2) unstable; urgency=low

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 May 2013 10:08:15 +0000

python-novaclient (2:2.13.0-1) experimental; urgency=low

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 02 Apr 2013 14:59:03 +0800

python-novaclient (2:2.10.0-1) experimental; urgency=low

  * New upstream version.
  * Remove exception_handling and prettytable patches (incorporated upstream).
  * Now using openstack-pkg-tools for the debian/rules.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Dec 2012 14:15:48 +0000

python-novaclient (1:2012.1-4) unstable; urgency=low

  * Added Breaks+Replaces: simh (<< 3.8.1-3) (Closes: #694372).

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 Nov 2012 19:28:18 +0000

python-novaclient (1:2012.1-3) unstable; urgency=low

  [ Ghe Rivero ]
  * Fixed FTBFS. Closes: #665036
  * Some depends and rules cleanup

  [ Mehdi Abaakouk ]
  * Add depend on python-pkg-resources
  * Better exception handling (Closes: 674451)
  * Fixed URL to the format 1.0 for the debian/copyright file.

 -- Ghe Rivero <ghe.rivero@stackops.com>  Thu, 07 Jun 2012 09:41:54 +0200

python-novaclient (1:2012.1-2) unstable; urgency=low

  * Fixed new prettytable api.

 -- Ghe Rivero <ghe.rivero@stackops.com>  Mon, 07 May 2012 10:14:01 +0200

python-novaclient (1:2012.1-1) unstable; urgency=low

  * New upstream release

 -- Ghe Rivero <ghe.rivero@stackops.com>  Mon, 09 Apr 2012 09:14:18 +0200

python-novaclient (1:2012.1~rc3-1) unstable; urgency=low

  * New upstream release

 -- Ghe Rivero <ghe.rivero@stackops.com>  Wed, 04 Apr 2012 10:54:47 +0200

python-novaclient (1:2012.1~rc1-1) unstable; urgency=low

  * New upstream release.

 -- Ghe Rivero <ghe.rivero@stackops.com>  Tue, 20 Mar 2012 13:11:41 +0100

python-novaclient (1:2012.1~e4-1) unstable; urgency=low

  * New upstream release

 -- Ghe Rivero <ghe@debian.org>  Fri, 02 Mar 2012 09:10:38 +0100

python-novaclient (1:2012.1~e3-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Thu, 26 Jan 2012 18:13:21 +0100

python-novaclient (1:2012.1~e2-4) unstable; urgency=low

  * Do not depends on python-argparse with python 2.7 (Closes: #653522)

 -- Julien Danjou <acid@debian.org>  Fri, 30 Dec 2011 09:56:20 +0100

python-novaclient (1:2012.1~e2-3) unstable; urgency=low

  [ Julien Danjou ]
  * Add URL of forwarded argparse patch

  [ Ghe Rivero ]
  * Fix Depends on python-argparse. (Closes: #653522)
  * Finish transition to dh_python2

 -- Julien Danjou <acid@debian.org>  Thu, 29 Dec 2011 09:48:28 +0100

python-novaclient (1:2012.1~e2-2) unstable; urgency=low

  * Clean egg-info on clean
  * Fix argparse requirement

 -- Julien Danjou <acid@debian.org>  Mon, 19 Dec 2011 17:19:54 +0100

python-novaclient (1:2012.1~e2-1) unstable; urgency=low

  * New upstream release

 -- Ghe Rivero <ghe@debian.org>  Fri, 16 Dec 2011 09:03:36 +0100

python-novaclient (2.6.7-1) unstable; urgency=low

  * [ce9adb4] Imported Upstream version 2.6.7

 -- Ghe Rivero <ghe@debian.org>  Fri, 04 Nov 2011 10:04:22 +0100

python-novaclient (2.4-2) unstable; urgency=low

  * Uploading to SID.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Jun 2011 07:37:52 +0000

python-novaclient (2.4-1) experimental; urgency=low

  * Initial release (Closes: #622944).

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Apr 2011 03:48:33 +0000
